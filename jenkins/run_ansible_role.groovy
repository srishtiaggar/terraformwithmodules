def installation_role(node, role) {
 ansible-playbook --private-key=/home/ubuntu/virginia.pem ansible_role/site.yml --ssh-extra-args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null' -e "host=${node} role=${role}"
}

return this
