provider "aws" {
  version  = "~> 2.0"
  shared_credentials_file = "~/.aws/credentials" 
  region = var.vpc_region
}

##########   VPC, SUBNET, ROUTE TABLES   #########

module "vpc" {
  source  = "../VPC"
  VPC_CIDR = var.vpc_cidr
  dnsSupport = var.dns_suppport
  dnsHostNames = var.dns_hostname
  publicSubnetCIDR = var.public_subnet_cidr
  mapPublicIP = var.map_Publicip
  availabilityZone = var.availablity_zone
  privateSubnetCIDR = var.private_subnet_cidr
  destinationCIDRblock = var.destination_cidr
  attachPublicIP = var.attach_publicip
}

###########  Security Group  ##############

module "Nginx_SG" {
  source  = "../security_group"
  sg_name = var.nginx_sg
  description = var.nginx_description
  vpc_id = module.vpc.vpc_id
  ingress_ports = var.nginx_ingress_port
  cidrs = var.cidr_block
}

module "jenkins_SG" {
  source  = "../security_group"
  sg_name = var.jenkins_sg
  description = var.jenkins_description
  vpc_id = module.vpc.vpc_id
  ingress_ports = var.jenkins_ingress_port
  cidrs = var.cidr_block
}

module "tomcat_SG" {
  source  = "../security_group"
  sg_name = var.tomcat_sg
  description = var.tomcat_description
  vpc_id = module.vpc.vpc_id
  ingress_ports = var.tomcat_ingress_port
  cidrs = var.cidr_block
}

module "bashion_SG" {
  source  = "../security_group"
  sg_name = var.bashion_sg
  description = var.bashion_description
  vpc_id = module.vpc.vpc_id
  ingress_ports = var.bashion_ingress_port
  cidrs = var.bashion_cidr_block
}

module "mysql_SG" {
   source  = "../mysql_securityGroup"
   sg_name = var.mysql_sg
   description =  var.mysql_description
   vpc_id = module.vpc.vpc_id
   to_ports = var.mysql_ingress_port
   from_ports = var.mysql_ingress_port
   source_security_group_id = [module.tomcat_SG.SG_id]
   ssg_id = [module.jenkins_SG.SG_id]
}

#####################     EC2 instance        ###################

module "nginx_instance" {
  source  = "../instances"
  instance_ami = var.ami_instance1
  instance_type = var.instance_type
  subnet_id = module.vpc.pub_subnet_id
  security_group = module.Nginx_SG.SG_id
  instance_key = var.key_name
  tag_name = "nginx"
}

module "jenkins_instance" {
  source  = "../instances"
  instance_ami = var.ami_instance2
  instance_type = var.instance_type
  subnet_id = module.vpc.pub_subnet_id
  security_group = module.jenkins_SG.SG_id
  instance_key = var.key_name
  tag_name = "jenkins"
}

module "tomcat_instance1" {
  source  = "../instances"
  instance_ami = var.ami_instance1
  instance_type = var.instance_type
  subnet_id = module.vpc.pub_subnet_id
  security_group = module.tomcat_SG.SG_id
  instance_key = var.key_name
  tag_name = "tomcat"
}

module "tomcat_instance2" {
  source  = "../instances"
  instance_ami = var.ami_instance1
  instance_type = var.instance_type
  subnet_id = module.vpc.pub_subnet_id
  security_group = module.tomcat_SG.SG_id
  instance_key = var.key_name
  tag_name = "tomcat"
}

module "mysql_instance" {
  source  = "../instances"
  instance_ami = var.ami_instance2
  instance_type = var.instance_type
  subnet_id = module.vpc.prv_subnet_id
  security_group = module.mysql_SG.SGid
  instance_key = var.key_name
  tag_name = "mysql"
}

module "bashion_instance" {
  source  = "../instances"
  instance_ami = var.ami_instance2
  instance_type = var.instance_type
  subnet_id = module.vpc.pub_subnet_id
  security_group = module.bashion_SG.SG_id
  instance_key = var.key_name
  tag_name = "bashion"
}
