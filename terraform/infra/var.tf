variable "vpc_region" {
  default = "us-east-1"
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "dns_suppport" {
  default = "true"
}

variable "dns_hostname" {
  default = "true"
}

variable "public_subnet_cidr" {
  default = "10.0.1.0/24"
}

variable "map_Publicip" {
  default = "true"
}

variable "availablity_zone" {
  default = "us-east-1a"
}

variable "private_subnet_cidr" {
  default = "10.0.2.0/24"
}

variable "destination_cidr" {
  default = "0.0.0.0/0"
}

variable "attach_publicip" {
  default = "false"
}

variable "nginx_sg" {
  default = "SG_nginx"
}

variable "nginx_description" {
  default = "security group for nginx"
}

variable "nginx_ingress_port" {
  type = list(string)
  default = ["22", "80", "8080"]
}

variable "cidr_block" {
  type = list(string)
  default = ["0.0.0.0/0"]
}

variable "jenkins_sg" {
  default = "SG_jenkins"
}

variable "jenkins_description" {
  default = "security group for jenkins"
}

variable "jenkins_ingress_port" {
  type = list(string)
  default = ["22", "8080"]
}

variable "tomcat_sg" {
  default = "SG_tomcat"
}

variable "tomcat_description" {
  default = "security group for tomcat"
}

variable "tomcat_ingress_port" {
  type = list(string)
  default = ["22", "8080", "80"]
}

variable "bashion_sg" {
  default = "SG_bastion"
}

variable "bashion_description" {
  default = "security group for bashion"
}

variable "bashion_ingress_port" {
  type = list(string)
  default = ["22"]
}

variable "bashion_cidr_block" {
  type = list(string)
  default = ["42.111.6.33/32"]
}

variable "mysql_sg" {
  default = "SG_mysql"
}

variable "mysql_description" {
  default = "security group for mysql"
}

variable "mysql_ingress_port" {
  default = "22,3306"
}

variable "ami_instance1" {
  default = "ami-04a4285f8613d8e70"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "Virginia_yahoo"
}

variable "ami_instance2" {
  default = "ami-0677a497187db098c"
}
