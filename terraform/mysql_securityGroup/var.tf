variable "sg_name" {
  default = ""
}
variable "description" {
  default = ""
}
variable "vpc_id" {
  default = "" 
}
variable "to_ports" {  
  default = "default_null"
}
variable "from_ports" {
  default = "default_null"
}
variable "source_security_group_id" {
  type = list(string)  
  default = []
}

variable "ssg_id" {
  type = list(string)
  default = []
}
