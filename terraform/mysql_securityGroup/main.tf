resource "aws_security_group" "mysqlSG" {
  name        = var.sg_name
  description = var.description
  vpc_id      = var.vpc_id
  tags = {
    Name = "mysql_sg"
  }
}
resource "aws_security_group_rule" "allow_worker_node" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "All egress traffic"
  security_group_id = aws_security_group.mysqlSG.id
}

resource "aws_security_group_rule" "tcp" {
  count                    = var.from_ports == "default_null" ? 0 : length(split(",", var.to_ports))
  type                     = "ingress"
  from_port                = element(split(",", var.from_ports), count.index)
  to_port                  = element(split(",", var.to_ports), count.index)
  protocol                 = "tcp"
  source_security_group_id = element(var.source_security_group_id, 0)
  description              = ""
  security_group_id        = aws_security_group.mysqlSG.id
}

resource "aws_security_group_rule" "another_tcp" {
  count                    = var.from_ports == "default_null" ? 0 : length(split(",", var.to_ports))
  type                     = "ingress"
  from_port                = element(split(",", var.from_ports), count.index)
  to_port                  = element(split(",", var.to_ports), count.index)
  protocol                 = "tcp"
  source_security_group_id = element(var.ssg_id, 0)
  description              = ""
  security_group_id        = aws_security_group.mysqlSG.id
}

