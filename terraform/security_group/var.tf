variable "sg_name" {
  default = ""
}
variable "description" {
  default = ""
}
variable "vpc_id" {
  default = ""
}
variable "ingress_ports" {
  type        = list(number)
  description = "list of ingress ports"
  default     = []
}
variable "cidrs" {
  type = "list"
  default = []
}

