variable "instance_ami" {
  default = ""
  }

variable "instance_type" {
  default = ""
  }
variable "security_group" {
  default = ""
  }
variable "instance_key" {
  default = ""
}
variable "subnet_id" {
  default = ""
}
variable "tag_name" {
  default = ""
}
