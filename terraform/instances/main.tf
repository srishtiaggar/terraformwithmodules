resource "aws_instance" "instance" {
  ami = var.instance_ami
  instance_type = var.instance_type
  key_name  = var.instance_key
  subnet_id = var.subnet_id
  vpc_security_group_ids = [var.security_group]
  tags = {
    Name = var.tag_name
   }
}
