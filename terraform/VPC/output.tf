output "vpc_id" {
  value = aws_vpc.HCL_VPC.id
}
output "pub_subnet_id" {
  value = aws_subnet.public_subnet.id
}
output "prv_subnet_id" {
  value = aws_subnet.private_subnet.id
}

