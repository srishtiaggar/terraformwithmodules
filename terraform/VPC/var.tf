variable "VPC_CIDR" {
  default = ""
}
variable "dnsSupport" {
  default = ""
}
variable "dnsHostNames" {
  default = ""
}
variable "publicSubnetCIDR" {
  default = ""
}

variable "mapPublicIP" {
  default = ""
}

variable "availabilityZone" {
  default = ""
}

variable "privateSubnetCIDR" {
  default = ""
}

variable "destinationCIDRblock" {
    default = ""
}

variable "attachPublicIP" {
  default = ""
}