#######   Create a VPC to launch instance   #########
resource "aws_vpc" "HCL_VPC" {
  cidr_block = var.VPC_CIDR
  enable_dns_support   = var.dnsSupport
  enable_dns_hostnames = var.dnsHostNames
  tags = {
        Name = "HCL VPC"
        }
}
########   Create a subnet to launch public instances  ##########
resource "aws_subnet" "public_subnet" {
  vpc_id                  = "${aws_vpc.HCL_VPC.id}"
  cidr_block              = var.publicSubnetCIDR
  map_public_ip_on_launch = var.mapPublicIP
  availability_zone       = var.availabilityZone
  tags = {
        Name = "Public Subnet"
        }
}

########   Create a subnet to launch private instances  ##########
resource "aws_subnet" "private_subnet" {
  vpc_id                  = "${aws_vpc.HCL_VPC.id}"
  cidr_block              = var.privateSubnetCIDR
  map_public_ip_on_launch = var.attachPublicIP
  availability_zone       = var.availabilityZone
  tags = {
        Name = "Private Subnet"
        }
}

#######   Create an internet gateway to give our subnet access    ##########
resource "aws_internet_gateway" "ig" {
  vpc_id = "${aws_vpc.HCL_VPC.id}"
  tags = {
        Name = "IGW"
        }
}

#######    Create Route Table to add IG   ##########
resource "aws_route_table" "public_rt" {
  vpc_id = "${aws_vpc.HCL_VPC.id}"
  route {
    cidr_block = var.destinationCIDRblock
    gateway_id  = "${aws_internet_gateway.ig.id}"
      }
  tags = {
    Name = "Public Route Table"
      }
}

########    Associate the public Route Table with the Subnet   ######
resource "aws_route_table_association" "publicRT_association" {
  subnet_id      = "${aws_subnet.public_subnet.id}"
  route_table_id = "${aws_route_table.public_rt.id}"
} 

##########  Create Elastic IP   ############
resource "aws_eip" "eip" {
vpc      = true
}

#########    Create NAT gateway   ###########
resource "aws_nat_gateway" "NAT" {
allocation_id = "${aws_eip.eip.id}"
subnet_id = "${aws_subnet.public_subnet.id}"
depends_on = ["aws_internet_gateway.ig"]
}

#######    Create Route Table to add NAT gatway   ##########
resource "aws_route_table" "private_rt" {
  vpc_id = "${aws_vpc.HCL_VPC.id}"
  route {
    cidr_block = var.destinationCIDRblock
    nat_gateway_id  = "${aws_nat_gateway.NAT.id}"
      }
  tags = {
    Name = "Private Route Table"
      }
}

########    Associate the private Route Table with the Subnet   ######
resource "aws_route_table_association" "PrivateRT_associate" {
  subnet_id = "${aws_subnet.private_subnet.id}"
  route_table_id = "${aws_route_table.private_rt.id}"
}
